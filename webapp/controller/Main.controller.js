/* global oComponent_pharmRep: true */
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "zmm_pharm_rep/model/models",
    "zmm_pharm_rep/model/formatter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Filter",
    "sap/ui/export/Spreadsheet",
    'sap/m/MessageBox'
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, models, formatter, FilterOperator, Filter, Spreadsheet, MessageBox) {
        "use strict";

        return Controller.extend("zmm_pharm_rep.controller.Main", {
            formatter: formatter,
            onInit: function () {
                oComponent_pharmRep._MainController = this;
                // this.getData();
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.getRoute('RouteMain').attachPatternMatched(this.onMainRoute, this);
            },
            onMainRoute: function (oEvent) {
                debugger;
                // var sUser = new sap.ushell.services.UserInfo().getUser().getId();
                var sUser = 'SAPIRA';
                this.getPilotCheck(sUser);
                try {
                    var MrpArea = oComponent_pharmRep.getComponentData().startupParameters["MrpArea"][0];
                } catch (e) {
                    var MrpArea = "";
                }
                oComponent_pharmRep.getModel("JSON").setProperty("/MrpArea", MrpArea);

            },
            getPilotCheck: function (sUser) {
                oComponent_pharmRep.getModel("ODATA").metadataLoaded().then(() => {
                    models.LoadPilotCheck(sUser).then(function (data) {
                        if (data.EvAuthorized) {
                            oComponent_pharmRep._MainController.getUserLgort();
                        }
                        else {
                            MessageBox.error(
                                oComponent_pharmRep.i18n('pilotMsg'),
                                {
                                    onClose: function (sAction) {
                                        oComponent_pharmRep._MainController.onNavBack();
                                    }
                                }
                            );
                        }


                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });

            },
            onOpenDialog: function (oEvent, sDialogName) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("zmm_pharm_rep.view.fragments." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onCloseDialog: function (oEvent, sDialogName) {
                try {
                    this[sDialogName].close();
                } catch (e) {
                    oComponent_VendCr[sDialogName].close();
                }

            },
            getUserLgort: function (sApp, sOrderNum) {
                var that = this;
                var bDefault = false;
                var sMrpArea = oComponent_pharmRep.getModel("JSON").getProperty("/MrpArea");
                oComponent_pharmRep.getModel("ODATA").metadataLoaded().then(() => {
                    models.LoadUserLgort().then(function (data) {
                        oComponent_pharmRep.getModel("JSON").setProperty("/userData", data.results);
                        if (!!sMrpArea) {
                            for (var i = 0; i < data.results.length; i++) {
                                if (data.results[i].Berid === sMrpArea) {
                                    bDefault = true;
                                    var sDefaultLgobe = data.results[i];
                                    oComponent_pharmRep.getModel("JSON").setProperty("/userDataDefault", sDefaultLgobe);

                                }

                            }
                        }
                        else {
                            for (var i = 0; i < data.results.length; i++) {
                                if (data.results[i].Default) {
                                    bDefault = true;
                                    var sDefaultLgobe = data.results[i];
                                    oComponent_pharmRep.getModel("JSON").setProperty("/userDataDefault", sDefaultLgobe);

                                }

                            }
                        }
                        if (data.results.length === 0 || !bDefault) {
                            oComponent_pharmRep.getModel("JSON").setProperty("/userDataDefault", { Lgort: '', Lgobe: '' })
                            oComponent_pharmRep._MainController.onOpenDialog('', 'ChangePharmacy');
                        }
                        else {
                            oComponent_pharmRep._MainController.getData();
                        }

                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });
            },
            getData: function () {
                var oModelData = oComponent_pharmRep.getModel("JSON").getData();
                oComponent_pharmRep.getModel("ODATA").metadataLoaded().then(() => {
                    models.loadReportData(oModelData.BeginDate, oModelData.EndDate, oModelData.userDataDefault.Lgort || '').then(function (data) {
                        var aStatus = [];
                        var aOrderTypes = [];
                        for (var i = 0; i < data.results.length; i++) {
                            data.results[i].LinesCountString = data.results[i].LinesCount.toString();
                            data.results[i].BedatString = oComponent_pharmRep._MainController.fixDate(data.results[i].Bedat);
                            aStatus.push({ StatusTxt: data.results[i].StatusText, Status: data.results[i].PoStatus });
                            aOrderTypes.push({ OrderTypeTxt: data.results[i].Batxt, OrderType: data.results[i].Bsart });
                        }
                        aStatus = oComponent_pharmRep._MainController.removeDuplicates(aStatus, "Status");
                        aOrderTypes = oComponent_pharmRep._MainController.removeDuplicates(aOrderTypes, "OrderType");
                        oComponent_pharmRep.getModel("JSON").setProperty("/status", aStatus);
                        oComponent_pharmRep.getModel("JSON").setProperty("/OrderTypes", aOrderTypes);
                        oComponent_pharmRep.getModel("JSON").setProperty("/orders", data.results);
                        oComponent_pharmRep.getModel("JSON").setProperty("/count", data.results.length);
                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });
            },
            fixDateTime: function (oDate) {
                var before = oDate.getTime();
                var after = new Date(before + 10800000);
                return after;
            },
            fixDate: function (oDate) {
                var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                    pattern: "dd.MM.yyyy"
                });
                var sNewDate = dateFormat.format(oDate);
                return sNewDate;
            },
            onChangeDate: function (oEvent) {
                var bValid = oEvent.getParameter("valid") && !!oEvent.getParameter("from") && !!oEvent.getParameter("to");
                var oEventSource = oEvent.getSource();
                oComponent_pharmRep.getModel("JSON").setProperty("/DateValueState", bValid ? 'None' : 'Error');
                // var sValue = oEvent.getParameter("newValue");
                oComponent_pharmRep.getModel("JSON").setProperty("/normalDate", bValid);
            },
            afterMessageViewClose: function (oEvent) {
                oComponent_pharmRep.getModel("JSON").setProperty("/BeginDateFilter", null);
                oComponent_pharmRep.getModel("JSON").setProperty("/EndDateFilter", null);
            },
            onOpenDateFilterDialog: function (oEvent) {
                oComponent_pharmRep.getModel("JSON").setProperty("/normalDate", true);
                oComponent_pharmRep.getModel("JSON").setProperty("/DateValueState", 'None');
                var sBeginDate = oComponent_pharmRep.getModel("JSON").getProperty("/BeginDate");
                var sEndDate = oComponent_pharmRep.getModel("JSON").getProperty("/EndDate");
                oComponent_pharmRep.getModel("JSON").setProperty("/BeginDateFilter", sBeginDate);
                oComponent_pharmRep.getModel("JSON").setProperty("/EndDateFilter", sEndDate);
            },
            fixDate: function (oDate) {
                var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                    pattern: "dd.MM.yyyy"
                });
                var sNewDate = dateFormat.format(oDate);
                return sNewDate;
            },
            removeDuplicates: function (array, key) {
                let lookup = new Set();
                return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
            },
            onPressChangeDate: function (oEvent) {
                // var sValue = oComponent_pharmRep.getModel("JSON").getProperty("/newDate");
                var oBeginDate = oComponent_pharmRep.getModel("JSON").getProperty("/BeginDateFilter");
                var oEndDate = oComponent_pharmRep.getModel("JSON").getProperty("/EndDateFilter");
                var sBeginDate = this.fixDate(oBeginDate);
                var sEndDate = this.fixDate(oEndDate);
                var dateTxt = sBeginDate + ' - ' + sEndDate;
                oComponent_pharmRep.getModel("JSON").setProperty("/BeginDate", this.fixDateTime(oBeginDate));
                oComponent_pharmRep.getModel("JSON").setProperty("/EndDate", oEndDate);
                oComponent_pharmRep.getModel("JSON").setProperty("/DateTxt", dateTxt);
                this.onCloseDialog('', 'DateFilter');
                this.getData();
            },
            onValueHelpSearch: function (oEvent, sType) {
                var sValue = oEvent.getParameter("value");
                var oFilter = [];
                if (sType === 'Lgobe') {
                    oFilter = new Filter({
                        filters: [new Filter("Lgobe", FilterOperator.Contains, sValue),
                        new Filter("Lgort", FilterOperator.Contains, sValue),
                        new Filter("Lgort", FilterOperator.Contains, sValue.toUpperCase())
                        ],
                        and: false
                    });

                }
                var oBinding = oEvent.getParameter("itemsBinding");
                oBinding.filter(oFilter);
            },
            onValueHelpClose: function (oEvent, sType) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);
                var userLgort = oComponent_pharmRep.getModel("JSON").getProperty("/userDataDefault/Lgort");

                if (!oSelectedItem) {
                    return;
                }
                if (sType === 'Lgobe') {
                    var oRow = oEvent.getParameter("selectedItem").getBindingContext("ODATA").getObject()
                    var sLgort = oRow.Lgort;
                    if (sLgort !== userLgort) {
                        oComponent_pharmRep._MainController.getData();
                    }
                    oComponent_pharmRep.getModel("JSON").setProperty("/userDataDefault", oRow);
                }
            },
            createColumnConfig: function () {
                var aCols = oComponent_pharmRep._MainController.byId("dataTable").getColumns();
                var aColumns = [];
                for (var i = 0; i < aCols.length - 1; i++) {
                    if (oComponent_pharmRep._MainController.byId("dataTable").getColumns()[i].getVisible()) {
                        aColumns.push({
                            label: aCols[i].getLabel().getProperty("text") || " ",
                            property: aCols[i].getFilterProperty() || " ",
                            width: '15rem'
                        });
                    }
                }
                return aColumns;

            },
            onExport: function () {
                var aCols, aItems, oSettings, oSheet;
                aCols = this.createColumnConfig();
                aItems = oComponent_pharmRep.getModel("JSON");
                var aIndices = oComponent_pharmRep._MainController.byId("dataTable").getBinding("rows").aIndices;
                var aSelectedModel = [];
                for (var i = 0; i < aIndices.length; i++) {
                    aSelectedModel.push(aItems.getProperty("/orders/" + aIndices[i]));
                }
                // var aSelectedModel = oComponent_pharmRep._MainController.byId("dataTable").getBinding("rows").oList;
                oSettings = {
                    workbook: {
                        columns: aCols
                    },
                    dataSource: aSelectedModel,
                    fileName: "הזמנות רכש"
                };

                oSheet = new Spreadsheet(oSettings);
                oSheet.build()
                    .then(function () { });

            },
            onNavBack: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({
                    target: { shellHash: "#" }
                });

            },
            onSearch: function (oEvent) {
                var oFilter = [];
                var filters = oComponent_pharmRep.getModel("JSON").getProperty("/filters");
                var sQuery = oEvent.getSource().getValue() || filters.OrderNumber;
                if (sQuery && sQuery.length > 0) {
                    oFilter.push(new Filter("Ebeln", FilterOperator.Contains, sQuery));

                }


                if (filters.Status && filters.Status.length) {
                    var aStatus = [];
                    for (const element of filters.Status) {
                        aStatus.push(new sap.ui.model.Filter("PoStatus", "EQ", element));
                    }
                    oFilter.push(new Filter(aStatus, false));
                }
                if (filters.OrderType && filters.OrderType.length) {
                    var aOrderType = [];
                    for (const element of filters.OrderType) {
                        aOrderType.push(new sap.ui.model.Filter("Bsart", "EQ", element));
                    }
                    oFilter.push(new Filter(aOrderType, false));
                }


                // update table binding
                var oTable = this.byId("dataTable");
                var oBinding = oTable.getBinding("rows");
                oBinding.filter(oFilter, "Application");
                oComponent_pharmRep.getModel("JSON").setProperty("/count", oBinding.getLength());
            },
            beforeNavTo: function (oEvent, orderNum, sApp, sMtart, sStatus, sBerid) {
                if (sApp === 'ZD') {
                    this.GoToME23N(oEvent, orderNum, 'X');
                }
                else if (!orderNum) {
                    this.goToCreatePharm(sBerid);
                }
                else if (sApp === 'ZB' && (sMtart === 'ZPRP' || sMtart === 'ZSPN') && sStatus !== '2') {
                    this.goToCreatePharm(sBerid);
                }
                else if (sApp === 'ZB' && (sMtart === 'ZPRP' || sMtart === 'ZSPN') && sStatus === '2') {
                    this.GoToME23N(oEvent, orderNum, 'X');
                }
                else if (sApp === 'ME9F') {
                    this.GoToME9F(oEvent, orderNum || '');
                }
                else {
                    if (sApp === 'ZC') {
                        sApp = 'ZB';
                    }
                    this.goToApp(oEvent, orderNum, sApp,sBerid);
                }


            },
            goToApp: function (oEvent, orderNum, sApp, sMrpArea) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_pharm_order_" + sApp.toLowerCase() + "-create";
                var actionURL = "#zmm_pharm_order_" + sApp.toLowerCase() + "-create?App=" + sApp + "&/Main/" + orderNum + "/" + sMrpArea;
                // var oParams = {
                //     OrderNumber: orderNum
                // };
                // this.setAppState(oCrossAppNavigator);
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done(function (aResponses) {
                        if (aResponses[semamticActionObj].supported === true) {
                            // var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                            //     target: {
                            //         semanticObject: "zmm_pharm_order_" + sApp.toLowerCase(),
                            //         action: actionURL
                            //     },
                            //     // params: oParams
                            // })) || "";
                            var hash = actionURL
                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                            // oCrossAppNavigator.toExternal({
                            //     target: {
                            //         shellHash: hash
                            //     }
                            // });
                        }

                    })
                    .fail(function () { });
            },
            goToCreatePharm: function (sBerid) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_create_pharm_or-create";
                var actionURL = "#zmm_create_pharm_or-create&/master/true/" + sBerid;
                // var oParams = {
                //     OrderNumber: orderNum
                // };
                // this.setAppState(oCrossAppNavigator);
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done(function (aResponses) {
                        if (aResponses[semamticActionObj].supported === true) {
                            // var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                            //     target: {
                            //         semanticObject: "zmm_pharm_order_" + sApp.toLowerCase(),
                            //         action: actionURL
                            //     },
                            //     // params: oParams
                            // })) || "";
                            var hash = actionURL
                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                            // oCrossAppNavigator.toExternal({
                            //     target: {
                            //         shellHash: hash
                            //     }
                            // });
                        }

                    })
                    .fail(function () { });
            },
            GoToME29N: function (event, sOrder) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "ME29N-display";
                var oParams = {
                    Order: sOrder,
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "ME29N",
                                    action: "display"
                                },
                                params: oParams
                            })) || "";
                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                            // oCrossAppNavigator.toExternal({
                            //     target: {
                            //         shellHash: hash
                            //     }
                            // });
                        }

                    })
                    .fail(function () { });
            },
            GoToME23N: function (event, sOrder, bEdit) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "MMPURPAMEPO-display";
                var oParams = {
                    PurchaseOrder: sOrder,
                    Edit: bEdit
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "MMPURPAMEPO",
                                    action: "display"
                                },
                                params: oParams
                            })) || "";
                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                            // oCrossAppNavigator.toExternal({
                            //     target: {
                            //         shellHash: hash
                            //     }
                            // });
                        }

                    })
                    .fail(function () { });
            },
            GoToME9F: function (event, sEbeln) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "ME9F-display";
                var oParams = {
                    IvEbeln: sEbeln,
                    IvEkgrp: 'Y99',
                    IvEkorg: '1100',
                    IvKschl: 'ZDRS'
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "ME9F",
                                    action: "display"
                                },
                                params: oParams
                            })) || "";
                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                            // oCrossAppNavigator.toExternal({
                            //     target: {
                            //         shellHash: hash
                            //     }
                            // });
                        }

                    })
                    .fail(function () { });
            }
        });
    });
