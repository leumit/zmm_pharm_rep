/* global QUnit */

sap.ui.require(["zmm_pharm_rep/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
