sap.ui.define([], function () {
    "use strict";
    return {
        addComma: function(value) {
			if (value) {
                value = parseFloat(value).toFixed(2);
				value = value.toString();
				return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			return value;
		},
        getOrderStatus: function(status){
            if(status === '1'){
                return 'None';
            }
            else if(status === '2'){
                return 'Error';
                
            }
            else if(status === '4'){
                return 'Information';
                
            }
            else if(status === '5'){ 
                return 'Success';
                
            }
            else if(status === '6'){ 
                return 'Warning';
                
            }
            else return 'None'
        }
    };
});
