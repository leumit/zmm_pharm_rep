sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device",
	"sap/ui/model/Filter"
], 
    /**
     * provide app-view type models (as in the first "V" in MVVC)
     * 
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel
     * @param {typeof sap.ui.Device} Device
     * 
     * @returns {Function} createDeviceModel() for providing runtime info for the device the UI5 app is running on
     */
    function (JSONModel, Device, Filter) {
        "use strict";

        return {
            createDeviceModel: function () {
                var oModel = new JSONModel(Device);
                oModel.setDefaultBindingMode("OneWay");
                return oModel;
        },
        createJSONModel: function () {
            var oModel = new sap.ui.model.json.JSONModel({
					BeginDate: new Date(new Date().setDate(new Date().getDate() - 90)),
					EndDate: new Date(),
                    lastDate: new Date(),
					userDataDefault: {
						Lgort:'',
						Lgobe:''
					},
					filters: {},
					normalDate: true,
                    DateTxt: String(new Date(new Date().setDate(new Date().getDate() - 90)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 90)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 90)).getFullYear()) + ' - ' + String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()),
					filterDateRB: 2
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		LoadUserLgort: function () {
            var aFilters = [];
			aFilters.push(new Filter("IvUname", sap.ui.model.FilterOperator.EQ, ''));
			return new Promise(function(resolve, reject) {
				oComponent_pharmRep.getModel("ODATA").read("/GetStoMrpListSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		LoadPilotCheck: function (sUser) {
			const sKey = oComponent_pharmRep.getModel("ODATA").createKey("/PilotCheckSet", {
				IvUserName: sUser
			});
			return new Promise(function(resolve, reject) {
				oComponent_pharmRep.getModel("ODATA").read(sKey, {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		loadReportData: function(BeginDate, EndDate,sLgort){
            var aFilters = [];
			aFilters.push(new Filter("IvFromDate", sap.ui.model.FilterOperator.EQ, BeginDate));			
			aFilters.push(new Filter("IvToDate", sap.ui.model.FilterOperator.EQ, EndDate));
            aFilters.push(new Filter("IvLgort", sap.ui.model.FilterOperator.EQ, sLgort));
			return new Promise(function(resolve, reject) {
				oComponent_pharmRep.getModel("ODATA").read("/GetReportDataSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
    };
});